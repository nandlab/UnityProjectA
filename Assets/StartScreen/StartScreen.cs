using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace StartScreen
{
    public class StartScreen : MonoBehaviour
    {
        void HandleInput()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
                return;
            }

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                SceneManager.LoadScene("AnimalDisco");
                return;
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                SceneManager.LoadScene("TextAdventureStartScreen");
                return;
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                SceneManager.LoadScene("TappyPlane");
                return;
            }
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                SceneManager.LoadScene("IdleGame");
                return;
            }
        }
        
        // Start is called before the first frame update
        void Start()
        {
            HandleInput();            
        }

        // Update is called once per frame
        void Update()
        {
            HandleInput();            
        }
    }
}
