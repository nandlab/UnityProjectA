using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AnimalDisco {
public class MaskedAnimal : MonoBehaviour
{
    const float SpeedY = 5.0f;
    float randomSpeedX;

    void Start()
    {
        randomSpeedX = (Random.value - 0.5f) * 3.0f;
    }

    void Update()
    {
        Vector3 position = transform.localPosition;
        position.x += Time.deltaTime * randomSpeedX;
        position.y -= Time.deltaTime * SpeedY;
        transform.localPosition = position;

        Camera mainCamera = Camera.main;
        Vector3 bottomViewPort = new Vector3(0.5f, 0.0f, mainCamera.nearClipPlane);
        Vector3 bottomWorldPoint = mainCamera.ViewportToWorldPoint(bottomViewPort);
        float minY = bottomWorldPoint.y - 1.5f;

        if (transform.position.y < minY) {
            Destroy(gameObject);
        }
    }
}
}
