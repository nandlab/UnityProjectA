using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AnimalDisco {
public class DiscoBackground : MonoBehaviour
{
    public bool Spin = false;
    
    SpriteRenderer sr;

    private void Scale()
    {
        float camHeight = 2 * Camera.main.orthographicSize;
        float camWidth = camHeight * Camera.main.aspect;
        float camDiagonal = Mathf.Sqrt(Mathf.Pow(camWidth, 2) + Mathf.Pow(camHeight, 2));
        float scale;
        if (sr.localBounds.size.x < sr.localBounds.size.y)
        {
            scale = camDiagonal / sr.localBounds.size.x;
        }
        else
        {
            scale = camDiagonal / sr.localBounds.size.y;
        }
        transform.localScale = scale * Vector3.one;
    }

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        Scale();
    }

    // Update is called once per frame
    void Update()
    {
        Scale();
        if (Spin) {
            transform.Rotate(new Vector3(0, 0, 45 * Time.deltaTime));
        }
    }
}
}
