using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AnimalDisco {
public class Dimmer : MonoBehaviour
{
    SpriteRenderer backgroundRenderer;
    int brightness;
    const int MAX_BRIGHTNESS = 10;
    const int MIN_BRIGHTNESS = 0;

    // Start is called before the first frame update
    void Start()
    {
        backgroundRenderer = transform.gameObject.GetComponent<SpriteRenderer>();
        brightness = MAX_BRIGHTNESS;
    }

    void SetColor() {
        Color color = Color.white * brightness / MAX_BRIGHTNESS;
        color.a = 1; // Alpha is always one
        backgroundRenderer.color = color;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha7)) {
            if (brightness > MIN_BRIGHTNESS)
                brightness -= 1;
            SetColor();
        }
        if (Input.GetKeyDown(KeyCode.Alpha8)) {
            if (brightness < MAX_BRIGHTNESS)
                brightness += 1;
            SetColor();            
        }
    }
}
}
