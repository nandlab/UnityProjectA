using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AnimalDisco {
public class DiscoLight : MonoBehaviour
{
    float duration;
    public Squidgame squidgame;

    static readonly Color greenLight = new Color(0, 1, 0, 1);
    static readonly Color redLight = new Color(1, 0, 0, 1);

    public void StartSquidGame()
    {
        GetComponent<SpriteRenderer>().color = greenLight;
    }

    public void GreenLight()
    {
        GetComponent<SpriteRenderer>().color = greenLight;
    }

    public void RedLight()
    {
        GetComponent<SpriteRenderer>().color = redLight;
    }

    void SetRandomColor()
    {
        Vector3 vect = Random.onUnitSphere;
        for (int i = 0; i < 3; i++)
        {
            vect[i] = Mathf.Abs(vect[i]);
        }
        Color color = new Color(vect[0], vect[1], vect[2], 1);
        GetComponent<SpriteRenderer>().color = color;
        duration = Random.Range(2.0f, 5.0f);
    }

    void SetSquidgameLight()
    {
        GetComponent<SpriteRenderer>().color = squidgame.RedLight ? redLight : greenLight;
    }

    void Start()
    {
        if (squidgame.Running)
        {
            SetSquidgameLight();
        }
        else
        {
            SetRandomColor();
        }
    }

    void Update()
    {
        if (squidgame.Running)
        {
            SetSquidgameLight();
        }
        else
        {
            duration -= Time.deltaTime;
            if (duration <= 0)
            {
                SetRandomColor();
            }
        }
    }
}
}
