using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TappyPlane {
    public class SpikeBlock : MovingObject
    {
        private bool phase = false;
        private const float scaleMax = 2.5f;
        private const float scaleMin = 2.0f;
        // Start is called before the first frame update
        new void Start()
        {
            base.Start();
            transform.localScale =  Vector3.one * scaleMin;
            Vector3 position = transform.localPosition;
            position.y = Random.Range(-1.5f, 1.5f);
            transform.localPosition = position;
        }

        // Update is called once per frame
        new void Update()
        {
            base.Update();

            float scaleModifier = Time.deltaTime;
            Vector3 scale = transform.localScale;
            if (phase)
            {
                scale += Vector3.one * scaleModifier;
                if (scale.x >= scaleMax)
                {
                    scale = Vector3.one * scaleMax;
                    phase = !phase;
                }
            }
            else
            {
                scale -= Vector3.one * scaleModifier;
                if (scale.x <= scaleMin)
                {
                    scale = Vector3.one * scaleMin;
                    phase = !phase;
                }
            }
            transform.localScale = scale;
        }
    }
}
