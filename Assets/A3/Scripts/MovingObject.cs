using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TappyPlane {
    public class MovingObject : MonoBehaviour
    {
        private SpriteRenderer sr;

        // Start is called before the first frame update
        protected void Start()
        {
            sr = GetComponent<SpriteRenderer>();
            Rigidbody2D rb = GetComponent<Rigidbody2D>();
            rb.velocity = new Vector2(-3, 0);
        }

        // Update is called once per frame
        protected void Update()
        {
            float leftEdge = Camera.main.ViewportToWorldPoint(Vector3.zero).x;
            if (transform.position.x < leftEdge - sr.bounds.size.x)
            {
                Destroy(gameObject);
            }
        }
    }
}
