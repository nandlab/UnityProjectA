using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TappyPlane {
    public class SpriteFullscreen : MonoBehaviour
    {
        private SpriteRenderer sr;
        Vector3 boxSize;

        private void Scale()
        {
            Vector3 camSize = Camera.main.ViewportToWorldPoint(Vector2.one);
            float scale;
            if (camSize.x > boxSize.x * camSize.y / boxSize.y)
            {
                scale = camSize.x / boxSize.x;
            }
            else
            {
                scale = camSize.y / boxSize.y;
            }
            transform.localScale = scale * Vector3.one;
        }

        // Start is called before the first frame update
        void Start()
        {
            sr = GetComponent<SpriteRenderer>();
            boxSize = sr.bounds.max;
            Scale();
        }

        // Update is called once per frame
        void Update()
        {
            Scale();
        }
    }
}
