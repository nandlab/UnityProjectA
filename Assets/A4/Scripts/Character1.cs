using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character1 : MonoBehaviour
{
    enum State {
        UP,
        DOWN
    }

    State state = State.UP;

    RectTransform rectTransform;
    RectTransform parentRectTransform;

    void LookUp()
    {
        rectTransform.localRotation = Quaternion.Euler(0, 0, 90.0f);
    }

    void LookDown()
    {
        rectTransform.localRotation = Quaternion.Euler(0, 0, -90.0f);
    }

    // Start is called before the first frame update
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        parentRectTransform = transform.parent.GetComponent<RectTransform>();
        Rect parentRect = parentRectTransform.rect;
        Vector2 position = new Vector2(Random.value * parentRect.width, Random.value * parentRect.height);
        rectTransform.anchoredPosition = position;
        if (Random.Range(0, 2) == 0)
        {
            state = State.UP;
            LookUp();
        }
        else
        {
            state = State.DOWN;
            LookDown();
        }
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = rectTransform.anchoredPosition;
        float y = position.y;

        switch (state)
        {
            case State.UP:
                y += Time.deltaTime * 100;
                if (y >= parentRectTransform.rect.height)
                {
                    LookDown();
                    y = parentRectTransform.rect.height;
                    state = State.DOWN;
                }
                break;
            case State.DOWN:
                y -= Time.deltaTime * 100;
                if (y <= 0)
                {
                    LookUp();
                    y = 0;
                    state = State.UP;
                }
                break;
            default:
                break;
        }

        position.y = y;
        rectTransform.anchoredPosition = position;
    }
}
