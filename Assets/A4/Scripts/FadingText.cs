using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace IdleGame
{
    public class FadingText : MonoBehaviour
    {
        private RectTransform rectTransform;
        private TextMeshProUGUI textMesh;

        // Start is called before the first frame update
        void Start()
        {
            rectTransform = GetComponent<RectTransform>();
            textMesh = GetComponent<TextMeshProUGUI>();
            textMesh.text = Text;
        }

        public string Text { get; set; }

        // Update is called once per frame
        void Update()
        {
            Vector3 pos = rectTransform.localPosition;
            pos.y += Time.deltaTime * 100;
            rectTransform.localPosition = pos;
            Color textColor = textMesh.color;
            float alpha = textColor.a;
            alpha -= Time.deltaTime * 0.75f;
            if (alpha <= 0.0f)
            {
                Destroy(gameObject);
                return;
            }
            textColor.a = alpha;
            textMesh.color = textColor;
        }
    }
}
