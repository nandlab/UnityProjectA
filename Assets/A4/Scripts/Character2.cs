using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character2 : MonoBehaviour
{
    RectTransform rectTransform;
    RectTransform parentRectTransform;

    enum State
    {
        RUN_TOWARD_CORNER,
        RUN_TOWARD_CENTER,
    }
    
    State state = State.RUN_TOWARD_CENTER;

    static int RandomDirection()
    {
        return Random.Range(0, 4);
    }

    // 0 is upper right corner and then it goes counter-clockwise.
    Vector2 DirectionToCorner()
    {
        Rect parentRect = parentRectTransform.rect;
        Vector2 corner = Vector2.zero;
        if (cornerDirection == 0 || cornerDirection == 1)
        {
            corner.y = parentRect.height;
        }
        if (cornerDirection == 0 || cornerDirection == 3)
        {
            corner.x = parentRect.width;
        }
        return corner;
    }
    
    int cornerDirection;

    // Start is called before the first frame update
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        parentRectTransform = transform.parent.GetComponent<RectTransform>();
        Rect parentRect = parentRectTransform.rect;
        cornerDirection = RandomDirection();
        Vector2 position = new Vector2(Random.value * parentRect.width, Random.value * parentRect.height);
        rectTransform.anchoredPosition = position;
        rectTransform.localEulerAngles = new Vector3(0, 0,
            Vector2ToAngle(RectCenter(parentRect) - position)
        );
    }

    static bool IsInsideRect(Vector2 pos, Rect rect)
    {
        return
            pos.x > 0 && pos.x < rect.width &&
            pos.y > 0 && pos.y < rect.height;
    }

    static Vector2 RectCenter(Rect rect)
    {
        return new Vector2(rect.width / 2, rect.height / 2);
    }

    static float Vector2ToAngle(Vector2 vector2)
    {
        return Mathf.Atan2(vector2.y, vector2.x) * Mathf.Rad2Deg;
    }

    static bool IsDestinationReached(Vector2 position, Vector2 direction, Vector2 destination)
    {
        return
            direction.x > 0 && (position.x - destination.x) >= 0 ||
            direction.x < 0 && (position.x - destination.x) <= 0 ||
            direction.y > 0 && (position.y - destination.y) >= 0 ||
            direction.y < 0 && (position.y - destination.y) <= 0;
    }

    // Update is called once per frame
    void Update()
    {
        Rect parentRect = parentRectTransform.rect;
        Vector2 position = rectTransform.anchoredPosition;
        Vector2 corner = DirectionToCorner();
        switch (state)
        {
            case State.RUN_TOWARD_CORNER:
            {
                Vector2 dir = corner - position;
                dir.Normalize();
                dir *= Time.deltaTime * 100;
                float angle = Vector2ToAngle(dir);
                rectTransform.localEulerAngles = new Vector3(0, 0, angle);
                Vector2 newPosition = position + dir;
                if ( !IsInsideRect(newPosition, parentRect) )
                {
                    position = corner;
                    state = State.RUN_TOWARD_CENTER;
                }
                else
                {
                    position = newPosition;
                }
                break;
            }
            case State.RUN_TOWARD_CENTER:
            {
                Vector2 center = RectCenter(parentRect);
                // float parentDiagonal = Mathf.Sqrt((Mathf.Pow(parentRect.height, 2) + Mathf.Pow(parentRect.width, 2)));
                Vector2 dir = center - position;
                dir.Normalize();
                dir *= Time.deltaTime * 100;
                float angle = Vector2ToAngle(dir);
                rectTransform.localEulerAngles = new Vector3(0, 0, angle);
                Vector2 newPosition = position + dir;
                // if ( (newPosition - corner).magnitude >= parentDiagonal / 2 - 0.1f )
                if ( IsDestinationReached(newPosition, dir, center) )
                {
                    position = center;
                    cornerDirection = RandomDirection();
                    state = State.RUN_TOWARD_CORNER;
                }
                else
                {
                    position = newPosition;
                }
                break;
            }
            default:
                break;
        }
        rectTransform.anchoredPosition = position;
    }
}
