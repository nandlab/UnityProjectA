using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.SceneManagement;

namespace IdleGame
{
    public class IdleGame : MonoBehaviour
    {
        [SerializeField] MyButton clickButton;
        [SerializeField] TextMeshProUGUI scoreText;
        [SerializeField] TextMeshProUGUI playtimeText;
        [SerializeField] TextMeshProUGUI passiveIncomeText;
        [SerializeField] GameObject prefabFadingText;
        [SerializeField] GameObject prefabCharacter1;
        [SerializeField] GameObject prefabCharacter2;

        [SerializeField] Upgrade upgradeClicker;
        [SerializeField] Upgrade upgradeCharacter1;
        [SerializeField] Upgrade upgradeCharacter2;
        [SerializeField] Transform characterAreaTransform;

        const int maxUpgradeAmount = 50;

        [System.Serializable]
        public class UpgradeUI
        {
            public Button button;
            public TextMeshProUGUI amount;
            public TextMeshProUGUI cost;
        }

        class UpgradeBinder
        {
            private Upgrade upgrade;
            private UpgradeUI upgradeUI;
            private int amount;

            public UpgradeBinder(Upgrade upgrade, UpgradeUI upgradeUI)
            {
                this.upgrade = upgrade;
                this.upgradeUI = upgradeUI;
                this.amount = 0;
            }

            public void SetClickListener(UnityAction action)
            {
                upgradeUI.button.onClick.AddListener(action);
            }

            public int Cost
            {
                get => upgrade.GetCost(amount);
            }

            public int Amount
            {
                get => amount;
                set => amount = value;
            }

            public string AmountText
            {
                get => upgradeUI.amount.text;
                set => upgradeUI.amount.text = value;
            }

            public string CostText
            {
                get => upgradeUI.cost.text;
                set => upgradeUI.cost.text = value;
            }

            public bool ButtonInteractable
            {
                get => upgradeUI.button.interactable;
                set => upgradeUI.button.interactable = value;
            }
        }

        [SerializeField] UpgradeUI upgradeClickerUI;
        [SerializeField] UpgradeUI upgradeCharacter1UI;
        [SerializeField] UpgradeUI upgradeCharacter2UI;

        UpgradeBinder upgradeClickerBinder;
        UpgradeBinder upgradeCharacter1Binder;
        UpgradeBinder upgradeCharacter2Binder;

        // Array of all the upgrade binders for convenience
        UpgradeBinder[] upgradeBinders;

        int score = 0;
        int passiveIncome = 0;
        int initialPlaytime = 0;
        int relativePlaytime = 0;

        // Start is called before the first frame update
        void Start()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene("StartScreen");
                return;
            }

            upgradeClickerBinder = new UpgradeBinder(upgradeClicker, upgradeClickerUI);
            upgradeCharacter1Binder = new UpgradeBinder(upgradeCharacter1, upgradeCharacter1UI);
            upgradeCharacter2Binder = new UpgradeBinder(upgradeCharacter2, upgradeCharacter2UI);

            upgradeBinders = new UpgradeBinder[]
            {
                upgradeClickerBinder,
                upgradeCharacter1Binder,
                upgradeCharacter2Binder
            };

            upgradeClickerBinder.SetClickListener(() =>
            {
                if (score >= upgradeClickerBinder.Cost)
                {
                    score -= upgradeClickerBinder.Cost;
                    upgradeClickerBinder.Amount++;
                    Debug.Log("Clicker upgraded");
                }
            });

            upgradeCharacter1Binder.SetClickListener(() =>
            {
                if (score >= upgradeCharacter1Binder.Cost)
                {
                    score -= upgradeCharacter1Binder.Cost;
                    upgradeCharacter1Binder.Amount++;
                    Instantiate(prefabCharacter1, characterAreaTransform);
                    passiveIncome += 5;
                    Debug.Log("Character 1 upgraded");
                }
            });

            upgradeCharacter2Binder.SetClickListener(() =>
            {
                if (score >= upgradeCharacter2Binder.Cost)
                {
                    score -= upgradeCharacter2Binder.Cost;
                    upgradeCharacter2Binder.Amount++;
                    Instantiate(prefabCharacter2, characterAreaTransform);
                    passiveIncome += 10;
                    Debug.Log("Character 2 upgraded");
                }
            });

            initialPlaytime = PlayerPrefs.GetInt("playtime", 0);
            relativePlaytime = 0;
            // clickButton.GetComponent<Button>().onClick.AddListener(Click);
            clickButton.onPointerDown.AddListener(Click);
            UpdateView();
        }

        void UpdateView()
        {
            UpdateUpgradeUI();
            UpdateScoreView();
            UpdatePlaytimeView();
            UpdatePassiveIncomeView();
        }

        void UpdateUpgradeUI()
        {
            foreach (UpgradeBinder upgradeBinder in upgradeBinders)
            {
                upgradeBinder.AmountText = $"{upgradeBinder.Amount}";
                if (upgradeBinder.Amount >= maxUpgradeAmount)
                {
                    upgradeBinder.ButtonInteractable = false;
                    upgradeBinder.CostText = "Maxed out!";
                    continue;
                }
                upgradeBinder.CostText = $"{upgradeBinder.Cost} €";
                if (score < upgradeBinder.Cost)
                {
                    upgradeBinder.ButtonInteractable = false;
                    continue;
                }
                upgradeBinder.ButtonInteractable = true;
            }
        }

        void UpdateScoreView()
        {
            scoreText.text = $"Balance: {score} €";
        }

        void UpdatePlaytimeView()
        {
            playtimeText.text = $"{initialPlaytime + relativePlaytime} s";
        }

        void UpdatePassiveIncomeView()
        {
            passiveIncomeText.text = $"Passive Income: {passiveIncome} €/s";
        }

        void Click(PointerEventData eventData)
        {
            int clickVal = upgradeClickerBinder.Amount + 1;
            score += clickVal;
            string fadingStr = $"+{clickVal}";
            Vector3 position = eventData.pressPosition;
            GameObject fadingText = Instantiate(prefabFadingText, position, Quaternion.identity, clickButton.transform);
            fadingText.GetComponent<FadingText>().Text = fadingStr;
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene("StartScreen");
                return;
            }

            int time = (int) Time.time;
            if (time > relativePlaytime)
            {
                relativePlaytime = time;
                int totalPlaytime = initialPlaytime + relativePlaytime;
                PlayerPrefs.SetInt("playtime", totalPlaytime);
                score += passiveIncome;
            }
            UpdateView();
        }
    }
}
