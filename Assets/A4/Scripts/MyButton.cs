using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine;

public class MyButton : Button
{
    public UnityEvent<PointerEventData> onPointerDown = new UnityEvent<PointerEventData>();

    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        onPointerDown.Invoke(eventData);
    }   
 }
