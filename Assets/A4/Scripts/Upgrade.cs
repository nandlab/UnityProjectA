using UnityEngine;
using System;

namespace IdleGame
{
    [CreateAssetMenu(fileName = "NewUpgrade", menuName = "ScriptableObjects/Upgrade", order = 1)]
    public class Upgrade : ScriptableObject
    {
        public double baseCost;
        public double multiplier;

        public Upgrade(double baseCost, double multiplier)
        {
            this.baseCost = baseCost;
            this.multiplier = multiplier;
        }

        public int GetCost(double upgrades)
        {
            return (int) (baseCost * Math.Pow(multiplier, upgrades));
        }
    }
}
