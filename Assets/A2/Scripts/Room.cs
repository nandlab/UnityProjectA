using UnityEngine;

namespace TextAdventure
{
    public class Room
    {
        public Room()
        {
            this.story = null;
            this.choices = null;
            this.sprites = null;
        }

        public Room(string story, Choice[] choices, Sprite[] sprites)
        {
            this.story = story;
            this.choices = choices;
            this.sprites = sprites;
        }

        private string story;
        private Choice[] choices;
        private Sprite[] sprites;

        public string Story {
            get => story;
            set => story = value;
        }

        public Choice[] Choices {
            get => choices;
            set => choices = value;
        }

        public Sprite[] Sprites {
            get => sprites;
            set => sprites = value;
        }
    }
}
