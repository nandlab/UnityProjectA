using System;

namespace TextAdventure
{
    public class Choice
    {
        public Choice()
        {
            this.description = null;
            this.enter = null;
        }

        public Choice(string description, Func<Room> enter)
        {
            this.description = description;
            this.enter = enter;
        }

        // Execute enterRoomAction and return entered room
        private string description;
        private Func<Room> enter;

        public string Description {
            get => description;
            set => description = value;
        }

        public Func<Room> Enter {
            get => enter;
            set => enter = value;
        }
    }
}
