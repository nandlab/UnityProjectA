using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Text;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

namespace TextAdventure {
    public class TextAdventure : MonoBehaviour
    {
        Room roomMain = new Room();
        Room roomTooHungry = new Room();
        Room roomWillEatPizza = new Room();
        Room roomOrderPizza = new Room();
        Room roomOutOfMoney = new Room();
        Room roomPizzaOutOfStock = new Room();
        Room roomSisterPizza = new Room();
        Room roomSisterCodeCompile = new Room();
        Room roomSisterCodeCompileBroke = new Room();
        Room roomSisterCodeDebug = new Room();
        Room roomSisterCodeStackOverflow = new Room();
        Room roomMakePizzaYourself = new Room();
        Room roomWin = new Room();

        bool pizzaEaten = false;
        int compileCounter = 0;

        [SerializeField] GameObject groupMain;
        CanvasGroup groupMainCanvas;
        [SerializeField] GameObject storyText;
        TextMeshProUGUI storyTextMesh;
        [SerializeField] GameObject choicesText;
        TextMeshProUGUI choicesTextMesh;
        [SerializeField] Image[] images;
        Room currentRoom;

        [SerializeField] Sprite spritePizzaIcon;
        [SerializeField] Sprite spritePizzaCook;
        [SerializeField] Sprite spriteProgrammer;
        [SerializeField] Sprite spritePizzaDelivery;
        [SerializeField] Sprite spriteError;
        [SerializeField] Sprite spriteWrench;
        [SerializeField] Sprite spriteLaptop;
        [SerializeField] Sprite spriteStackOverflow;
        [SerializeField] Sprite spriteFire;
        [SerializeField] Sprite spriteClock;
        [SerializeField] Sprite spriteCustomerSupport;
        [SerializeField] Sprite spritePoor;
        [SerializeField] Sprite spriteFacePalm;
        [SerializeField] Sprite spriteCelebration;
        [SerializeField] GameObject groupWalkthrough;
        CanvasGroup groupWalkthroughCanvas;
        bool showingWalkthrough = false;

        int GetChoice(int numberOfChoices)
        {
            for (int i = 0; i < numberOfChoices; i++)
            {
                if (Input.GetKeyDown(KeyCode.Alpha1 + i))
                {
                        return i;
                }
            }
            return -1;
        }

        void InitGame()
        {
            pizzaEaten = false;
            compileCounter = 0;
        }

        void UpdateGameView()
        {
            /* StringBuilder sb = new StringBuilder($"{currentRoom.story}");
            for (int i = 0; i < currentRoom.possibleChoices.Length; i++)
            {
                sb.Append($"\n({i}) {currentRoom.possibleChoices[i].description}");
            }
            storyTextMesh.text = sb.ToString(); */

            storyTextMesh.text = currentRoom.Story;

            if (currentRoom.Choices is null)
            {
                choicesTextMesh.text = "";
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < currentRoom.Choices.Length; i++)
                {
                    if (i > 0)
                    {
                        sb.Append('\n');
                    }
                    sb.Append($"({i+1}) {currentRoom.Choices[i].Description}");
                }
                choicesTextMesh.text = sb.ToString();
            }

            for (int i = 0; i < 2; i++)
            {
                if (currentRoom.Sprites is not null && i < currentRoom.Sprites.Length)
                {
                    images[i].gameObject.SetActive(true);
                    images[i].sprite = currentRoom.Sprites[i];
                    // images[i].SetNativeSize();
                }
                else
                {
                    images[i].gameObject.SetActive(false);
                }
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            InitGame();

            Choice[] choicesTryAgain = new Choice[] {
                new Choice("Try again", () => {
                    InitGame();
                    return roomMain;
                })
            };

            Func<Room> CompileFunc = () => {
                if (++compileCounter >= 10)
                {
                    return roomSisterCodeCompileBroke;
                }
                return roomSisterCodeCompile;
            };

            roomMain.Story =
                "You have to create a text adventure game until tomorrow but haven't done anything yet.\n\n" +
                "<b>Text Adventure (13 Points)</b>\n" +
                "Implement a text adventure that tells a story of your own making...";
            roomMain.Choices = new Choice[]
            {
                new Choice("Do the task", () => pizzaEaten ? roomWin : roomTooHungry),
                new Choice("Eat pizza", () => roomWillEatPizza)
            };
            roomMain.Sprites = new Sprite[]
            {
                spritePizzaIcon,
                spriteProgrammer
            };
            
            roomTooHungry.Story =
                "You tried to concentrate on the task but you cannot stop thinking about pizza every minute.";
            roomTooHungry.Choices = new Choice[]
            {
                new Choice("Think again", () => roomMain)
            };
            roomTooHungry.Sprites = new Sprite[]
            {
                spritePizzaIcon,
                spriteClock
            };

            roomWillEatPizza.Story =
                "You are going to eat a pizza.";
            roomWillEatPizza.Choices = new Choice[]
            {
                new Choice("Spend money and save time", () => roomOrderPizza),
                new Choice("Spend time and save money", () => roomSisterPizza),
                new Choice("Think again", () => roomMain)
            };
            roomWillEatPizza.Sprites = new Sprite[]
            {
                spritePizzaIcon,
                spritePizzaCook
            };

            roomOrderPizza.Story =
                "<b>Welcome to tastypizza.com</b>\n\n" +
                "Using our Online Menu you can custom " +
                "order a pizza with your favorite toppings and choose from a variety of sauces, " +
                "cheeses and crusts. Your pizza creation will be hot and ready to pick up in 30 " +
                "minutes! Be creative! Satisfy your craving!\n\n" +
                "Phone: +4910293102";
            roomOrderPizza.Choices = new Choice[]
            {
                new Choice("<b>\"Margherita\"</b>: $9.99", () => roomPizzaOutOfStock),
                new Choice("<b>\"Formaggio\"</b>: $10.99", () => roomPizzaOutOfStock),
                new Choice("<b>\"Hawaii\"</b>: $11.99", () => roomPizzaOutOfStock),
                new Choice("<b>\"Premium\"</b>: $29.99", () => roomOutOfMoney),
                new Choice("Consider another pizza option", () => roomWillEatPizza)
            };
            roomOrderPizza.Sprites = new Sprite[]
            {
                spritePizzaIcon,
                spritePizzaDelivery
            };

            roomOutOfMoney.Story = "<color=red>Hah! You realy think you can afford that!</color>";
            roomOutOfMoney.Choices = choicesTryAgain;
            roomOutOfMoney.Sprites = new Sprite[]
            {
                spritePizzaIcon,
                spritePoor
            };

            roomPizzaOutOfStock.Story =
                "\"Sorry to tell you that this pizza is currently out of stock! Please choose another variant.\"";
            roomPizzaOutOfStock.Choices = new Choice[]
            {
                new Choice("Back to the menu", () => roomOrderPizza)
            };
            roomPizzaOutOfStock.Sprites = new Sprite[]
            {
                spritePizzaIcon,
                spriteCustomerSupport
            };

            roomSisterPizza.Story =
                "You asked your sister to make a pizza.\n\n" +
                "She agreed to make one if you help her with her programming homework.\n\n" +
                "You look at her code and notice it is full of bugs.";
            roomSisterPizza.Choices = new Choice[]
            {
                new Choice("Hit compile button", CompileFunc),
                new Choice("Debug the code yourself", () => roomSisterCodeDebug),
                new Choice("Use StackOverflow", () => roomSisterCodeStackOverflow),
                new Choice("Make a pizza yourself", () => roomMakePizzaYourself),
                new Choice("Consider another pizza option", () => roomWillEatPizza)
            };
            roomSisterPizza.Sprites = new Sprite[]
            {
                spritePizzaIcon,
                spriteLaptop
            };

            roomSisterCodeCompile.Story = "The compilation fails with 94 errors and 41 warnings.";
            roomSisterCodeCompile.Choices = new Choice[]
            {
                new Choice("Consider something else...", () => roomSisterPizza),
                new Choice("Compile again", CompileFunc)
            };
            roomSisterCodeCompile.Sprites = new Sprite[]
            {
                spritePizzaIcon,
                spriteError,
            };

            roomSisterCodeCompileBroke.Story = "<color=red>You smashed the buttons so many times you broke her keyboard. Well done!</color>";
            roomSisterCodeCompileBroke.Choices = choicesTryAgain;
            roomSisterCodeCompileBroke.Sprites = new Sprite[]
            {
                spritePizzaIcon,
                spriteFacePalm
            };

            roomSisterCodeDebug.Story = "<color=red>You were debugging the code all day and made no progress.</color>";
            roomSisterCodeDebug.Choices = choicesTryAgain;
            roomSisterCodeDebug.Sprites = new Sprite[]
            {
                spritePizzaIcon,
                spriteWrench
            };

            roomSisterCodeStackOverflow.Story =
                "You have found the solution to the problem in the first search result " +
                "and fixed the bug.\n\n" +
                "Your sister is happy and made you a delicious pizza.";
            roomSisterCodeStackOverflow.Choices = new Choice[]
            {
                new Choice("Enjoy the pizza", () => {
                    pizzaEaten = true;
                    return roomMain;
                }),
            };
            roomSisterCodeStackOverflow.Sprites = new Sprite[]
            {
                spritePizzaIcon,
                spriteStackOverflow
            };

            roomMakePizzaYourself.Story = "<color=red>You burned the house down.</color>";
            roomMakePizzaYourself.Choices = choicesTryAgain;
            roomMakePizzaYourself.Sprites = new Sprite[]
            {
                spritePizzaIcon,
                spriteFire
            };

            roomWin.Story = "<color=green>You successfully write a game about pizza!</color>";
            roomWin.Choices = new Choice[]
            {
                new Choice("Play again", () => {
                    InitGame();
                    return roomMain;
                })
            };
            roomWin.Sprites = new Sprite[]
            {
                spritePizzaIcon,
                spriteCelebration
            };

            // TextMeshProUGUI[] textMeshes = GetComponentsInChildren<TextMeshProUGUI>();
            // Debug.Assert(textMeshes.Length >= 2);
            storyTextMesh = storyText.GetComponent<TextMeshProUGUI>();
            choicesTextMesh = choicesText.GetComponent<TextMeshProUGUI>();
            // images = GetComponentsInChildren<Image>().Where(e => e.tag == "AssetImage").ToArray();
            Debug.Assert(images.Length >= 2);
            currentRoom = roomMain;
            Debug.Assert(currentRoom != null);
            groupMainCanvas = groupMain.GetComponent<CanvasGroup>();
            groupWalkthroughCanvas = groupWalkthrough.GetComponent<CanvasGroup>();
            // groupMainCanvas.enabled = false;
            UpdateGameView();
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene("TextAdventureStartScreen");
                return;
            }

            if (Input.GetKeyDown(KeyCode.Tab))
            {
                if (showingWalkthrough)
                {
                    showingWalkthrough = false;
                    groupWalkthroughCanvas.alpha = 0;
                    groupWalkthroughCanvas.interactable = false;
                    groupWalkthroughCanvas.blocksRaycasts = false;
                }
                else
                {
                    showingWalkthrough = true;
                    groupWalkthroughCanvas.alpha = 1;
                    groupWalkthroughCanvas.interactable = true;
                    groupWalkthroughCanvas.blocksRaycasts = true;
                }
            }

            if (!showingWalkthrough)
            {
                int n = GetChoice(currentRoom.Choices.Length);

                if (n >= 0)
                {
                    currentRoom = currentRoom.Choices[n].Enter();
                    UpdateGameView();
                }
            }
        }
    }
}
